package ca.mcgill.mchi.common.pophr.model;

import org.semanticweb.owlapi.model.OWLNamedIndividual;

/**
 * Known Stratification
 */
public final class Stratification {
  private final OWLNamedIndividual ref;
  private final String shortForm;
  private final Type type;

  public Stratification(OWLNamedIndividual ref, String shortForm, Type type) {
    this.ref = ref;
    this.shortForm = shortForm;
    this.type = type;
  }

  /**
   * Associated Ontology Reference
   *
   * @return Reference
   */
  public OWLNamedIndividual getRef() {
    return ref;
  }

  /**
   * Gets the shortform representation
   *
   * @return Shortform
   */
  public String getShortForm() {
    return shortForm;
  }

  /**
   * Dimension of the stratification
   *
   * @return Dimension
   */
  public Type getType() {
    return type;
  }

  @Override
  public String toString() {
    return ref.getIRI().getRemainder().or("anon");
  }

  /**
   * Possible Stratification Dimensions
   */
  public enum Type {
    AGE, GEO, SEX
  }
}

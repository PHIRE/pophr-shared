package ca.mcgill.mchi.common.pophr.model;

import ca.mcgill.mchi.common.pophr.interfaces.*;
import org.apache.commons.lang3.NotImplementedException;
import org.semanticweb.owlapi.model.OWLClass;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PHIndicator implements Indicator, KbElement {
  @SuppressWarnings({"unused", "FieldCanBeLocal"})
  private final OWLClass id;
  @SuppressWarnings({"unused", "FieldCanBeLocal"})
  private final Unit displayUnit;
  @SuppressWarnings({"unused", "FieldCanBeLocal"})
  private final Unit sourceUnit;
  @SuppressWarnings({"unused", "FieldCanBeLocal"})
  private final AvailableData availableData;
  @SuppressWarnings({"unused", "FieldCanBeLocal"})
  private final AvailableMeth availableDocuments;

  private final transient List<PHConcept> concepts = new ArrayList<>();
  private final transient List<DataCube> cubes = new ArrayList<>();

  /**
   * Creates an Indicator
   * @param id KB Element reference
   * @param src Source unit
   * @param dst Visualization unit
   * @param cubes All cubes for this indicator.
   */
  public PHIndicator(OWLClass id, Unit src, Unit dst, List<DataCube> cubes, Collection<Methodology> documents) {
    this.id = id;
    this.displayUnit = dst;
    this.sourceUnit = src;
    this.availableData = AvailableData.fromCubes(cubes);
    availableDocuments = AvailableMeth.fromDocuments(documents);
  }

  /**
   * Gets all data package for this indicator.
   *
   * @return Data Packages
   */
  @Override public List<DataCube> getCubes() { return cubes; }

  /**
   * Get what concept this indicator is indicating (ex. Obesity for the BMI indicator)
   *
   * @return PHConcept
   */
  @Override public List<PHConcept> getIndicatorOf() { return concepts; }

  /**
   * Get all labels which refers to this indicator (labels, synonyms, equivalencies, etc)
   *
   * @return All Labels
   */
  @Override public List<String> getLabels() { throw new NotImplementedException("Not yet supported"); }

  /**
   * Short form representation of the concept reference (ex. phio:neuropathy)
   *
   * @return Short form ID representation.
   */
  public String getShortForm() { throw new NotImplementedException("Not yet supported"); }

  /**
   * Ontology Reference for the indicator.
   *
   * @return KB Reference
   */
  @Override public OWLClass getRef() { return id; }
}

package ca.mcgill.mchi.common.pophr.model;

import java.util.Optional;

public class ReferenceObservations {
  protected final Stratification age, geo, sex;
  protected final String tableName;
  private final String representation;

  public ReferenceObservations(
      Stratification age,
      Stratification geo,
      Stratification sex,
      String tableName,
      String representation) {
    this.sex = sex;
    this.age = age;
    this.geo = geo;
    this.tableName = tableName;
    this.representation = representation;
  }

  /**
   * Stratification associated with this observation, if present
   *
   * @return Age Stratification
   */
  public Optional<Stratification> getAge() {
    return Optional.ofNullable(age);
  }

  /**
   * Stratification associated with this observation, if present
   *
   * @return Geo Stratification
   */
  public Optional<Stratification> getGeo() {
    return Optional.ofNullable(geo);
  }

  /**
   * Stratification associated with this observation, if present
   *
   * @return Sex Stratification
   */
  public Optional<Stratification> getSex() {
    return Optional.ofNullable(sex);
  }

  /**
   * Gets the name of the table that stores this reference observation.
   *
   * @return Table Name
   */
  public String getTableName() {
    return tableName;
  }

  @Override
  public String toString() {
    return representation;
  }
}

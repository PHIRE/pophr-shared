package ca.mcgill.mchi.common.pophr.interfaces;

import java.util.Optional;
import java.util.stream.Stream;

public interface Concept extends KbElement {
  /**
   * Gets the default indicator of this concept.
   * @return The default indicator if present
   */
  Optional<Indicator> getDefaultIndicator();

  /**
   * List indicators for this concept.
   * @return Series of indicators
   */
  Stream<Indicator> getIndicators();
}

package ca.mcgill.mchi.common.pophr.model;

import ca.mcgill.mchi.common.pophr.interfaces.IMeasure;
import ca.mcgill.mchi.common.pophr.interfaces.IMethod;
import ca.mcgill.mchi.common.pophr.interfaces.ISuppressionStrategy;
import org.apache.commons.lang3.NotImplementedException;
import org.semanticweb.owlapi.model.OWLEntity;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Representation of a Method (ex. Prevalence (countOfCases/countOfPeople))
 */
public class Method implements IMethod {
  private final int id;
  private final String name;
  private final String[] inputs;
  private final String[] details;
  private final String value;

  private final Collection<IMeasure> newInputs;
  private final Collection<IMeasure> newDetails;
  private final IMeasure newValue;

  /**
   * Creates a method from its internal data.
   * @param id Unique ID
   * @param name Human Readable Name
   * @param inputs Inputs to the method algorithm
   * @param details Additional method details
   * @param value Main computed value
   */
  public Method(int id, String name, String[] inputs, String[] details, String value) {
    this.id = id;
    this.name = name;
    this.inputs = inputs;
    this.details = details;
    this.value = value;

    this.newInputs = Stream.of(this.inputs).map(in -> new Measure(in, IMeasure.DataType.number)).collect(Collectors.toList());
    this.newDetails = Stream.of(this.details).map(in -> new Measure(in, IMeasure.DataType.number)).collect(Collectors.toList());
    this.newValue = new Measure(value, IMeasure.DataType.number);
  }

  /**
   * Required inputs to the method algorithm
   *
   * @return Array of inputs (shortform remainder)
   */
  public String[] getOldInputs() {
    return inputs;
  }

  /**
   * Additional method details provided
   * @return Array of additional outputs
   */
  public String[] getOldDetails() {
    return details;
  }

  /**
   * Main computed value
   * @return Main value
   */
  public String getOldValue() {
    return value;
  }

  /**
   * Required inputs to the method algorithm
   *
   * @return Array of inputs (shortform remainder)
   */
  public Collection<IMeasure> getInputs() { return newInputs; }

  /**
   * Additional method details provided
   * @return Array of additional outputs
   */
  public Collection<IMeasure> getDetails() { return newDetails; }

  /**
   * Main computed value
   * @return Main value
   */
  public IMeasure getValue() { return newValue; }

  /**
   * Applies this method to a set of data.
   * <p>
   * Each time the apply is called, it should process exactly one observation.
   *
   * @param ins      Collections of observation properties.
   * @param outs     Collections of observation properties.
   * @param strategy Strategy to handle suppression of data if needed.
   */
  @Override
  public void apply(List<Object> ins, List<Object> outs, ISuppressionStrategy strategy) {
    throw new NotImplementedException("Not Implemented Yet");
  }

  @Override
  public Object[] stdApply(List<List<Number>> ins, List<List<Number>> std, ISuppressionStrategy strategy) {
    throw new NotImplementedException("Not Implemented Yet");
  }

  /**
   * Unique ID
   *
   * @return UID
   */
  public int getId() {
    return id;
  }

  /**
   * Human Readable Name
   *
   * @return Name
   */
  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Method)) return false;
    Method method = (Method) o;
    return getId() == method.getId();
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
    return name;
  }

  /**
   * Ontology Reference for the indicator.
   *
   * @return KB Reference
   */
  public OWLEntity getRef() {
    throw new NotImplementedException("Not Implemented Yet");
  }
}

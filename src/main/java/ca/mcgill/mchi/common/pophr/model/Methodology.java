package ca.mcgill.mchi.common.pophr.model;

import java.sql.Timestamp;
import java.util.Locale;

/**
 * A model representing methodology documentation for a specific indicator.
 */
public class Methodology {
  private transient final byte[] data;
  private final String locale;
  private final String hash;
  private final String shortForm;
  private final Timestamp timestamp;

  public Methodology(String shortform, Timestamp edited, byte[] files, String hash, String locale) {
    this.shortForm = shortform;
    this.timestamp = edited;
    this.data = files;
    this.hash = hash;
    this.locale = locale;
  }

  /**
   * Gets the file data.
   *
   * @return Content of the PDF.
   */
  public byte[] getData() {
    return data;
  }

  /**
   * Gets the locale associated with that PDF.
   *
   * @return PDF locale
   */
  public String getLocale() {
    return locale;
  }

  /**
   * Constructor for a Java Locale Object.
   *
   * @return The Java Locale.
   */
  public Locale getJavaLocale() {
    return new Locale(locale);
  }

  /**
   * Unique identifier of the file content.
   *
   * @return The hash of the file.
   */
  public String getHash() {
    return hash;
  }

  /**
   * Short form encoding of the indicator reference.
   *
   * @return shortForm encoded indicator.
   */
  public String getShortForm() {
    return shortForm;
  }

  /**
   * Timestamp of the last file edition.
   *
   * @return UNIX Timestamp
   */
  public Timestamp getTimestamp() {
    return timestamp;
  }

  @Override
  public String toString() {
    return hash.substring(0, 7) + " Locale." + new Locale(locale).getDisplayName();
  }
}
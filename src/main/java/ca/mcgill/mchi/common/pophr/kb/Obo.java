package ca.mcgill.mchi.common.pophr.kb;

public final class Obo {
  public static final String ns = "http://purl.obolibrary.org/obo/";
  public static final String prefix = "obo";
}

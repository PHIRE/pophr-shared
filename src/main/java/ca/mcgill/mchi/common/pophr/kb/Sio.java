package ca.mcgill.mchi.common.pophr.kb;

import org.semanticweb.owlapi.model.OWLAnnotationProperty;

import static ca.mcgill.mchi.common.pophr.kb.KBUtils.aprop;

public final class Sio {
  public static final String ns = "http://semanticscience.org/resource/";
  public static final String prefix = "sio";
  public static final OWLAnnotationProperty synonym = aprop(ns, "synonym");
}

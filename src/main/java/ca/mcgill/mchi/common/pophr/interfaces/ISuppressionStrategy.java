package ca.mcgill.mchi.common.pophr.interfaces;

/**
 * Suppression Strategies
 */
public interface ISuppressionStrategy {
  /**
   * Apply the Stratification Strategy
   * @param counts Counts to consider in the strategy.
   * @return Return true if we should suppress.
   */
  boolean shouldSuppress(long counts);
}

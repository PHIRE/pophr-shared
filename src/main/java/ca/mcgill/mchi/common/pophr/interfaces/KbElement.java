package ca.mcgill.mchi.common.pophr.interfaces;

import org.semanticweb.owlapi.model.OWLEntity;

/**
 * An element in our knowledge base
 */
public interface KbElement {
  /**
   * Ontology Reference for the indicator.
   *
   * @return KB Reference
   */
  OWLEntity getRef();
}

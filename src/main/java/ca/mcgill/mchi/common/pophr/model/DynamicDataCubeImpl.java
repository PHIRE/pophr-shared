package ca.mcgill.mchi.common.pophr.model;

import ca.mcgill.mchi.common.pophr.interfaces.Indicator;

import java.util.Collection;
import java.util.Optional;

public class DynamicDataCubeImpl implements DataCube {
  private final Integer uid;
  private final Aggregator aggregatingFunc;
  private final String computeCubeScript;
  private final Indicator dataFor;
  private final Collection<? extends FactTable> dependencies;
  private final Method method;
  private final ReferenceObservations reference;
  private final String tableName;

  public DynamicDataCubeImpl(
      Integer uid,
      Aggregator aggregatingFunc,
      Indicator dataFor,
      Method method,
      ReferenceObservations reference,
      String tableName,
      String computeCubeScript,
      Collection<? extends FactTable> dependencies) {

    if (aggregatingFunc == null)
      throw new IllegalStateException("Dynamic Data Cube needs to be aggregatable.");

    this.uid = uid;
    this.aggregatingFunc = aggregatingFunc;
    this.dataFor = dataFor;
    this.method = method;
    this.reference = reference;
    this.tableName = tableName;
    this.computeCubeScript = computeCubeScript;
    this.dependencies = dependencies;
  }

  /**
   * Aggregator function to use (null means not-aggregatable)
   *
   * @return Aggregator
   */
  @Override
  public Optional<Aggregator> getAggregator() {
    return Optional.of(aggregatingFunc);
  }

  /**
   * Get all dependant facts
   *
   * @return Dependencies
   */
  @Override
  public Collection<? extends FactTable> getFactDependencies() {
    return dependencies;
  }

  /**
   * Get the indicator this data was loaded for.
   *
   * @return Indicator
   */
  @Override
  public Indicator getIndicator() {
    return dataFor;
  }

  /**
   * Gets the method to be used when processing the data.
   *
   * @return Method
   */
  @Override
  public Method getMethod() {
    return method;
  }

  /**
   * Gets the reference observation
   *
   * @return Reference Observation
   */
  @Override
  public ReferenceObservations getReferenceObs() {
    return reference;
  }

  /**
   * Name of the table in the DB.
   *
   * @return Table Name
   */
  @Override
  public String getTableName() {
    return tableName;
  }

  /**
   * Unique ID of this Cube
   *
   * @return Unique ID
   */
  @Override
  public Integer getUid() {
    return uid;
  }

  /**
   * Gets the cube computation script.
   *
   * @return SQL Script
   */
  public String getComputeCubeScript() {
    return computeCubeScript;
  }
}

package ca.mcgill.mchi.common.pophr.model;

import ca.mcgill.mchi.common.pophr.interfaces.Concept;
import ca.mcgill.mchi.common.pophr.interfaces.Indicator;
import ca.mcgill.mchi.common.pophr.kb.KBUtils;
import ca.mcgill.mchi.common.pophr.kb.Phio;
import org.apache.commons.lang3.Validate;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.util.DefaultPrefixManager;

import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Stream;

/**
 * An element of the causal map. Can be a disease, condition, determinants, ...
 *
 * This class is used to interact with the ontology around what is known on a disease or condition.
 * It serves as a facade that provides a clearer interface for the retrieval of ontology concepts.
 */
public class PHConcept implements Concept {
  private final OWLClass id;
  @SuppressWarnings("unused") // Used during serialization
  private final OWLClass defaultIndicator;
  private final Collection<Indicator> indicators;

  // FIXME(malavv): Slowly try to remove this shortForm.
  private transient final String shortForm;

  public PHConcept(OWLClass owl, String shortForm) {
    this.id = owl;
    this.shortForm = shortForm;
    this.defaultIndicator = null;
    this.indicators = Collections.emptyList();
  }

  public PHConcept(OWLClass owl, String shortForm, @Nullable Indicator defaultIndicator, Collection<Indicator> indicators) {
    this.id = owl;
    this.shortForm = shortForm;
    this.defaultIndicator = defaultIndicator != null ? defaultIndicator.getRef().asOWLClass() : null;
    this.indicators = indicators;
  }

  /**
   * Gets the ontology reference of this disease or risk factor.
   *
   * @return Reference
   */
  public OWLClass getRef() {
    return id;
  }

  /**
   * Gets a shortform ID
   *
   * @return Shortform
   */
  public String getShortForm() {
    return shortForm;
  }

  @Override public int hashCode() { return Objects.hash(id); }
  @Override public String toString() { return String.format("PHConcept{id='%s'}", shortForm); }
  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    PHConcept PHConcept = (PHConcept) o;
    return Objects.equals(id, PHConcept.id);
  }

  /**
   * Gets the default indicator of this concept.
   *
   * @return The default indicator if present
   */
  @Override public Optional<Indicator> getDefaultIndicator() {
    return Optional.ofNullable(defaultIndicator)
        .flatMap(id -> indicators.stream().filter(i -> i.getRef().equals(id)).findAny());
  }

  /**
   * List indicators for this concept.
   *
   * @return Series of indicators
   */
  @Override public Stream<Indicator> getIndicators() { return indicators.stream(); }

  /**
   * Retrieves all the disease and risk factors that are indicated by an indicator.
   *
   * @param ind Indicator
   * @param o   Ontology
   * @param pm  Prefix Manager
   * @return Diseases and Risk Factors.
   */
  public static Stream<PHConcept> getIndicated(Indicator ind, OWLOntology o, DefaultPrefixManager pm) {
    Validate.notNull(ind, "No indicator provided to get the concept of");
    Validate.notNull(ind.getRef(), "No indicator does not reference the ontology");
    Validate.notNull(o, "No Knowledge Base Provided");
    Validate.notNull(pm, "No Short Form Encoder Provided");

    return KBUtils.getSuperClasses(ind.getRef().asOWLClass(), o)
        .filter(PHConcept::isIndicatorOfExpr)
        .map(PHConcept::getIndicatedConcept)
        // Filter to make sure the indicated concept is a valid owl class.
        .filter(Optional::isPresent).map(Optional::get)
        .map(owl -> new PHConcept(owl, pm.getShortForm(owl)));
  }

  /**
   * Is the expression of the form :indicatorOf some :Thing
   *
   * @param oce The Expression to test
   * @return True if it matches.
   */
  private static boolean isIndicatorOfExpr(OWLClassExpression oce) {
    return oce instanceof OWLObjectSomeValuesFrom
        && ((OWLObjectSomeValuesFrom) oce).getProperty().equals(Phio.indicatorOf);
  }

  /**
   * Returns the indicated PHConcept, assuming this is a valid expression.
   *
   * @param isIndicatorOfExpr Expression to extract the concept from.
   * @return Valid named Class or empty if it is an expression.
   */
  private static Optional<OWLClass> getIndicatedConcept(OWLClassExpression isIndicatorOfExpr) {
    OWLClassExpression owl = ((OWLObjectSomeValuesFrom) isIndicatorOfExpr).getFiller();
    return owl.isClassExpressionLiteral() ? Optional.of(owl.asOWLClass()) : Optional.empty();
  }
}

package ca.mcgill.mchi.common.pophr.kb;

import org.semanticweb.owlapi.model.OWLAnnotationProperty;

import static ca.mcgill.mchi.common.pophr.kb.KBUtils.aprop;

public final class Phont {
  public static final String ns = "http://surveillance.mcgill.ca/km/PublicHealth/phont.owl#";
  public static final String prefix = "phont";

  public static final OWLAnnotationProperty dspSynonym = aprop(ns, "DSP_synonym");
}

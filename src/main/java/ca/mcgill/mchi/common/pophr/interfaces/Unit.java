package ca.mcgill.mchi.common.pophr.interfaces;

import org.semanticweb.owlapi.model.*;

/**
 * Unit of our data
 * <p>
 *   Source is in the DB (usually per unit)
 *   Destination is how to present (usually per 1000 units)
 * </p>
 */
public interface Unit extends KbElement {
  /**
   * Get the Unit Type
   * @return Type
   */
  OWLClass getType();

  /**
   * By how much to multiply the source data before presentation
   * @return Conversion factor.
   */
  double getConversionFactor();
}

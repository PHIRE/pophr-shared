package ca.mcgill.mchi.common.pophr.interfaces;

import java.util.Collection;
import java.util.List;

public interface IMethod {

  /**
   * Measures that contributes additional details.
   * @return List of measures including additional details.
   */
  Collection<IMeasure> getDetails();

  /**
   * List measures to be used for the inputs.
   * @return Collection of inputs.N
   */
  Collection<IMeasure> getInputs();

  /**
   * Gets the main value for this method.
   * @return Measure of the central value.
   */
  IMeasure getValue();

  /**
   * Applies this method to a set of data.
   *
   * Each time the apply is called, it should process exactly one observation.
   * @param ins Collections of observation properties.
   * @param outs Collections of observation properties.
   * @param strategy Strategy to handle suppression of data if needed.
   */
  void apply(List<Object> ins, List<Object> outs, ISuppressionStrategy strategy);

  Object[] stdApply(List<List<Number>> ins, List<List<Number>> std, ISuppressionStrategy strategy);
}

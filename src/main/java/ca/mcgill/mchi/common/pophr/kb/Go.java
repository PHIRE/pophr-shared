package ca.mcgill.mchi.common.pophr.kb;

import org.semanticweb.owlapi.model.OWLAnnotationProperty;

import static ca.mcgill.mchi.common.pophr.kb.KBUtils.aprop;

public final class Go {
  public static final String ns = "http://www.geneontology.org/formats/oboInOwl#";
  public static final String prefix = "go";

  public static final OWLAnnotationProperty hasExactSynonym = aprop(ns, "hasExactSynonym");
  public static final OWLAnnotationProperty hasNarrowSynonym = aprop(ns, "hasNarrowSynonym");
  public static final OWLAnnotationProperty hasRelatedSynonym = aprop(ns, "hasRelatedSynonym");
}

package ca.mcgill.mchi.common.pophr.interfaces;

import ca.mcgill.mchi.common.pophr.model.DataCube;
import ca.mcgill.mchi.common.pophr.model.PHConcept;

import java.util.List;

/**
 * Represents an indicator of a disease ex. prevalence of diabetes.
 * <p>
 * It does not necessarily as associated data.
 */
public interface Indicator extends KbElement {
  /**
   * Gets all data package for this indicator.
   *
   * @return Data Packages
   */
  List<DataCube> getCubes();

  /**
   * Get what concept this indicator is indicating (ex. Obesity for the BMI indicator)
   *
   * @return PHConcept
   */
  List<PHConcept> getIndicatorOf();

  /**
   * Get all labels which refers to this indicator (labels, synonyms, equivalencies, etc)
   *
   * @return All Labels
   */
  List<String> getLabels();
}

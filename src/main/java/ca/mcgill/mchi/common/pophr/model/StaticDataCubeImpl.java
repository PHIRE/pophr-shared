package ca.mcgill.mchi.common.pophr.model;

import ca.mcgill.mchi.common.pophr.interfaces.Indicator;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

/**
 * This class represents static data that was given "as-is" for some indicator.
 * <p>
 * For example, in the context of a survey, it makes sense that data is just collected and than "given" to us in a
 * static way.
 */
public class StaticDataCubeImpl implements DataCube {
  private final Integer uid;
  private final Aggregator aggregatingFunc;
  private final Indicator dataFor;
  private final Method method;
  private final ReferenceObservations reference;
  private final String tableName;

  public StaticDataCubeImpl(
      Integer uid,
      Aggregator aggregatingFunc,
      Indicator dataFor,
      Method method,
      ReferenceObservations reference,
      String tableName) {
    this.uid = uid;
    this.aggregatingFunc = aggregatingFunc;
    this.dataFor = dataFor;
    this.method = method;
    this.reference = reference;
    this.tableName = tableName;
  }

  /**
   * Gets what aggregator (if any) should be used for this data. If none is set, data is not aggregatable.
   *
   * @return Aggregator if present.
   */
  @Override
  public Optional<Aggregator> getAggregator() {
    return Optional.ofNullable(aggregatingFunc);
  }

  /**
   * Get all dependant facts
   *
   * @return Dependencies
   */
  @Override
  public Collection<? extends FactTable> getFactDependencies() {
    return Collections.emptyList();
  }

  /**
   * Gets the indicator this data is associated with.
   *
   * @return Indicator Reference.
   */
  @Override
  public Indicator getIndicator() {
    return dataFor;
  }

  /**
   * Gets the method (ex. prev, incidence, identity) used to extract data out of this cube data.
   *
   * @return The Method.
   */
  @Override
  public Method getMethod() {
    return method;
  }

  /**
   * Gets the reference observation this data is associated with.
   *
   * @return Associated Reference Observation.
   */
  @Override
  public ReferenceObservations getReferenceObs() {
    return reference;
  }

  /**
   * Name of the table containing the data that was given, in the "data" schema.
   *
   * @return Name of the table.
   */
  @Override
  public String getTableName() {
    return tableName;
  }

  /**
   * Unique ID of this Cube
   *
   * @return Unique ID
   */
  @Override
  public Integer getUid() {
    return uid;
  }
}

package ca.mcgill.mchi.common.pophr.model;

/**
 * Data about some facts about some people.
 * <p>
 * Usually for the purpose of pre-computation. Things like when exactly did someone became incident.
 */
public interface FactTable {
  /**
   * One-Line description for the Fact Table.
   *
   * @return Human Readable Description
   */
  String getName();

  /**
   * Gets a Unique Identifier for this fact table.
   *
   * @return Unique ID.
   */
  int getUid();
}

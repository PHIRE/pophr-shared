package ca.mcgill.mchi.common.pophr.model;

import ca.mcgill.mchi.common.pophr.interfaces.IMeasure;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLEntity;
import uk.ac.manchester.cs.owl.owlapi.OWLClassImpl;

public class Measure implements IMeasure {
  private final OWLClass ref;
  private transient String columnName;
  private final DataType datatype;

  public Measure(String columnName, DataType dataType) {
    this.columnName = columnName;
    this.datatype = dataType;
    this.ref = new OWLClassImpl(IRI.create("unimplemented:" + columnName));
  }

  public String getColumnName() { return columnName; }
  public DataType getDataType() { return datatype; }

  @Override public OWLEntity getRef() { return ref; }
  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Measure measure = (Measure) o;
    if (!columnName.equals(measure.columnName)) return false;
    return datatype == measure.datatype;
  }
  @Override public int hashCode() {
    int result = columnName.hashCode();
    result = 31 * result + datatype.hashCode();
    return result;
  }
  @Override public String toString() { return String.format("Measure<%s:%s>", columnName, datatype); }
}

package ca.mcgill.mchi.common.pophr.kb;

import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.search.EntitySearcher;
import uk.ac.manchester.cs.owl.owlapi.OWLAnnotationPropertyImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLClassImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectPropertyImpl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class KBUtils {

  /**
   * Returns an appropriate english label for this concept.
   *
   * @param ref      PHConcept
   * @param ontology Ontology
   * @return Appropriate concept if present.
   */
  public static Optional<String> getEnOrDefaultLabel(final OWLClass ref, final OWLOntology ontology) {
    Optional<String> label = EntitySearcher.getAnnotations(ref, ontology.getImportsClosure(), Rdfs.label).stream()
        .map(oa -> oa.getValue().asLiteral())
        .filter(o -> o.isPresent()).map(o -> o.get())
        .filter(v -> !v.hasLang() || v.hasLang("en")).map(OWLLiteral::getLiteral)
        .findFirst();

    return Optional.ofNullable(label.orElse(ref.getIRI().getRemainder().orNull()));
  }

  public static Optional<String> getEnOrDefaultLabel(final OWLNamedIndividual ref, final OWLOntology ontology) {
    return EntitySearcher.getAnnotations(ref, ontology.getImportsClosure(), Rdfs.label).stream()
        .map(oa -> oa.getValue().asLiteral())
        .filter(o -> o.isPresent()).map(o -> o.get())
        .filter(v -> !v.hasLang() || v.hasLang("en")).map(OWLLiteral::getLiteral)
        .findFirst();
  }

  static OWLObjectProperty oprop(String ns, String rhs) {
    return new OWLObjectPropertyImpl(IRI.create(ns, rhs));
  }

  static OWLAnnotationProperty aprop(String ns, String rhs) {
    return new OWLAnnotationPropertyImpl(IRI.create(ns, rhs));
  }

  static OWLClass nclass(String ns, String rhs) {
    return new OWLClassImpl(IRI.create(ns, rhs));
  }

  static OWLAnnotationProperty annot(String ns, String rhs) {
    return new OWLAnnotationPropertyImpl(IRI.create(ns, rhs));
  }

  public static Predicate<OWLClassExpression> isSomeExpr() {
    return oce -> oce.getClassExpressionType().equals(ClassExpressionType.OBJECT_SOME_VALUES_FROM);
  }

  public static Predicate<OWLClassExpression> hasFillerClassLit() {
    return oce -> oce instanceof HasFiller && ((HasFiller) oce).getFiller() instanceof OWLClassExpression;
  }

  /**
   * Walks the Graph and Identify all Superclasses.
   * <p>
   * WARNING! Since this is a graph walking function, we can't determine
   *
   * @param owl      Concept Reference
   * @param ontology KB
   * @return Stream of distinct direct and indirect parent class expression.
   */
  public static Stream<OWLClassExpression> getSuperClasses(OWLClassExpression owl, OWLOntology ontology) {
    return owl.isClassExpressionLiteral()
        ? EntitySearcher.getSuperClasses(owl.asOWLClass(), ontology.getImportsClosure()).stream()
        .flatMap(oce -> Stream.concat(Stream.concat(
            Stream.of(oce),
            oce.isClassExpressionLiteral() ? EntitySearcher.getEquivalentClasses(oce.asOWLClass(), ontology.getImportsClosure()).stream() : Stream.empty()),
            getSuperClasses(oce, ontology)))
        .distinct()
        : Stream.empty();
  }

  private static List<OWLAnnotationProperty> getAllLabelProperties() {
    return Arrays.asList(Sio.synonym, Phont.dspSynonym, Go.hasExactSynonym, Go.hasNarrowSynonym, Go.hasRelatedSynonym);
  }

  public static List<String> allLabels(OWLClass owl, OWLOntology ontology) {
    final Set<OWLOntology> importsClosure = ontology.getImportsClosure();

    return KBUtils.getAllLabelProperties().stream()
        .map(la -> EntitySearcher.getAnnotations(owl, importsClosure, la))
        .map(st -> st.stream().filter(lbl -> lbl.getValue() instanceof OWLLiteral && lbl.getValue().asLiteral().isPresent()))
        .flatMap(stream -> stream)
        .map(oa -> {
          final String prop = oa.getProperty().getIRI().getRemainder().or("n/a");
          final String val = oa.getValue().asLiteral().transform(Object::toString).or("n/a");
          return String.format("[%s]%s", prop, val);
        }).collect(Collectors.toList());
  }

  /**
   * Use flatMap to only keep axiom of the form "hasUnit value 1_per"
   * @param axiom Axiom to test
   * @return Associated has value axiom if present
   */
  public static Stream<OWLObjectHasValue> keepObjectHasValue(OWLClassExpression axiom) {
    return axiom.getClassExpressionType().equals(ClassExpressionType.OBJECT_HAS_VALUE)
        ? Stream.of((OWLObjectHasValue)axiom) : Stream.empty();
  }
}

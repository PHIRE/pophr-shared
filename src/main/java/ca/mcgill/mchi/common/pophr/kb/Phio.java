package ca.mcgill.mchi.common.pophr.kb;

import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLObjectProperty;

import static ca.mcgill.mchi.common.pophr.kb.KBUtils.*;

public final class Phio {
  public static final String ns = "http://surveillance.mcgill.ca/km/Indicators/HealthIndicators.owl#";
  public static final String prefix = "phio";
  public static final OWLObjectProperty indicatorOf = oprop(ns, "indicatorOf");
  public static final OWLAnnotationProperty specificIndicator = aprop(ns, "isSpecificIndicator");
  public static final OWLClass ageStratification = nclass(ns, "AgeStratification");
  public static final OWLClass geoStratification = nclass(ns, "GeoStratification");
  public static final OWLClass sexStratification = nclass(ns, "GenderStratification");
}

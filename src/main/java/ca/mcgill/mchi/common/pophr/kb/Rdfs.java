package ca.mcgill.mchi.common.pophr.kb;

import org.openrdf.model.vocabulary.RDFS;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;

import static ca.mcgill.mchi.common.pophr.kb.KBUtils.annot;

public final class Rdfs {
  public static final String ns = RDFS.NAMESPACE;
  public static final String prefix = "rdfs";

  public static OWLAnnotationProperty label = annot(ns, RDFS.LABEL.getLocalName());
}

package ca.mcgill.mchi.common.pophr.interfaces;

public interface IMeasure extends KbElement {
  enum DataType {
    string,
    number,
    Boolean
  }
  String getColumnName();
  DataType getDataType();
}

package ca.mcgill.mchi.common.pophr.interfaces;

import ca.mcgill.mchi.common.pophr.model.DataCube;

import java.util.List;

public final class AvailableData {

  public final List<DataCube> cubes;

  private AvailableData(List<DataCube> cubes) {
    this.cubes = cubes;
  }

  public static AvailableData fromCubes(List<DataCube> cubes) {
    return new AvailableData(cubes);
  }
}

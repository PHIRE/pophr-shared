package ca.mcgill.mchi.common.pophr.model;

import java.util.stream.Stream;

/**
 * Type of aggregators that can be used for rolling up data cubes.
 */
public enum Aggregator {
  SUM("sum");

  private final String id;

  Aggregator(String id) {
    this.id = id;
  }

  /**
   * Gets an Aggregator by its ID.
   *
   * @param id The ID of the requested aggregator (can be null)
   * @return Found aggregator or null.
   */
  public static Aggregator fromId(String id) {
    if (id == null)
      return null;
    return Stream.of(Aggregator.values())
        .filter(agg -> agg.getId().equalsIgnoreCase(id))
        .findFirst()
        .orElse(null);
  }

  /**
   * Gets the String ID of the aggregator as understood by PopHR.
   *
   * @return String ID.
   */
  public String getId() {
    return id;
  }
}

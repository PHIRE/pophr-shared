package ca.mcgill.mchi.common.pophr.model;

import ca.mcgill.mchi.common.pophr.interfaces.IMethod;
import ca.mcgill.mchi.common.pophr.interfaces.Indicator;

import java.util.Collection;
import java.util.Optional;

/**
 * Represents a data package associated with an indicator.
 */
public interface DataCube {
  /**
   * Aggregator function to use (null means not-aggregatable)
   *
   * @return Aggregator
   */
  Optional<Aggregator> getAggregator();

  /**
   * Get all dependant facts
   *
   * @return Dependencies
   */
  Collection<? extends FactTable> getFactDependencies();

  /**
   * Get the indicator this data was loaded for.
   *
   * @return Indicator
   */
  Indicator getIndicator();

  /**
   * Gets the method to be used when processing the data.
   *
   * @return Method
   */
  IMethod getMethod();

  /**
   * Gets the reference observation
   *
   * @return Reference Observation
   */
  ReferenceObservations getReferenceObs();

  /**
   * Name of the table in the DB.
   *
   * @return Table Name
   */
  String getTableName();

  /**
   * Unique ID of this Cube
   *
   * @return Unique ID
   */
  Integer getUid();
}

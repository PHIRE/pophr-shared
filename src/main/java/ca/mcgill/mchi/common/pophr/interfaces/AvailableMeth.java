package ca.mcgill.mchi.common.pophr.interfaces;

import ca.mcgill.mchi.common.pophr.model.Methodology;

import java.util.Collection;

public final class AvailableMeth {

  public final Collection<Methodology> documents;

  private AvailableMeth(Collection<Methodology> documents) {
    this.documents = documents;
  }

  public static AvailableMeth fromDocuments(Collection<Methodology> cubes) {
    return new AvailableMeth(cubes);
  }
}
